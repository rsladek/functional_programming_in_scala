package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
    * Exercise 1
    */
  def pascal(column: Int, row: Int): Int = (row, column) match {
    case (r, c) if c == r => 1
    case (r, c) if c == 0 || c == r + 1 => 1
    case (r, c) => pascal(c - 1, r - 1) + pascal(c, r - 1)
  }


  /**
    * Exercise 2
    */
  def balance(chars: List[Char]): Boolean = {

    def collectParentheses(chars: List[Char], parentheses: List[Char]): List[Char] = {
      (chars, parentheses) match {
        case (Nil, p) => p
        case (c, p) if List('(', ')').contains(c.head) => collectParentheses(c.tail, p :+ c.head)
        case (c, p) => collectParentheses(c.tail, p)
      }
    }

    def checkPairs(parentheses: String): Boolean = parentheses match {
      case "" => true
      case ")" | "(" => false
      case ")(" => false
      case s => checkPairs(s.replace("()", ""))
    }
    checkPairs(collectParentheses(chars, List()).mkString)
  }

  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]) = {
    val memo = Map[Int, Int]()
    def countMemoized(money: Int, numberCoins: Int, coins: List[Int]): Int = {
      memo get money match {
        case Some(a) => a
        case None => {
          val n: Int = (money, numberCoins) match {
            case (0, _) => 1
            case (n, _) if n < 0 => 0
            case (n, m) if n >= 1 && m <= 0 => 0
            case (n, m) => countMemoized(money, m - 1, coins) + countMemoized(n - coins(m - 1), m, coins)
          }
          memo + (money -> n)
          n
        }
      }

    }
    countMemoized(money, coins.size, coins)
  }
}
