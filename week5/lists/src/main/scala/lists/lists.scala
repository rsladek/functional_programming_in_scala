package lists


object ListUtils {

  def reverse[T](xs: List[T]): List[T] = xs match {
    case List() => xs
    case head :: tail => reverse(tail) ::: List(head)
  }
}
