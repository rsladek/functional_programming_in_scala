package lists

import org.scalatest.FunSuite

class ListUtilsTest extends FunSuite {

  test("revert simple list") {
    val inputList = List("a", "b", "c")
    val actual = ListUtils.reverse(inputList)
    val expected = List("c", "b", "a")
    assertResult(expected)(actual)
  }

  test("revert empty list") {
    val inputList = List()
    val actual = ListUtils.reverse(inputList)
    val expected = List()
    assertResult(expected)(actual)
  }

  test("revert very long list") {
    val inputList = (1 to 99999).toList
    val actual = ListUtils.reverse(inputList)
    val expected = (99999 to 1).toList
    assertResult(expected)(actual)

  }

}
